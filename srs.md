# 1. Introduction
### 1.1 Purpose 
This SRS describes the requirements and specifications of e-commerce site. It explains the functional features, along with interface details, design constraints and other issues.

### 1.2 Document Conventions
Highlighted line for functional requirements. Blue colored text for essential links. Underlined text will be considered as most important and must to read. 


### 1.3 Intended Audience and Reading Suggestions
This document is for users, designers, developers, testers and owner of this software. It will give themselves a clear concept about what to made, how to made and purpose of this software. It will also highlights time, budget and tools constrains. Rest of the SRS contains overall description of this software, user interface design, functional requirements, non functional requirements, Hardware and software interfaces and constrains. 

### 1.4 Product Scope
For Most of the e-commerce site, there is no option for negotiation though customers want to negotiate. To solve this problem this software has an intelligent negotiator. It negotiate with customers without any human supervisory. So customer will get 24 hours service. As a e-commerce software it will save customer time cost and labor.

### 1.5 References
> 830-1984 - IEEE Guide for Software Requirements Specifications


# 2. Overall Description
### 2.1	Product Perspective
As mentioned earlier this software has an intelligent negotiator which will be active 24 hour for the service of customers. For selling a product we have to know customers willingness to pay (WTP). Its AI trained negotiator will determine customer WTP and negotiate intelligently. It can be an open platform for buyers and sellers where they can buy and sell their good by opening an account.

### 2.2	Product Functions
- Getting user information
- user cart
- user wish-list
- appear desired goods
- active online negotiator
- Orders
- sale�s list

### 2.3	User Classes and Characteristics
Users are people who know basic levels of English and know simple browsing. 

### 2.4	Operating Environment
This software operates well form any personal computer and mobile devices. Any browser will supports its all features. Online negotiator will be published as an application for android devices too. 

### 2.5	Design and Implementation Constraints
Android app for online negotiator developers may face some difficulties while working on android studio. Because it needs large amount of ram to operate. 

### 2.6	User Documentation
User will get some demo for operating this software. There will be a help button for any kind of help and they can rate any product, its sellers and can give reviews.

### 2.7	Assumptions and Dependencies
This software may be depended on some codes for Artificial Intelligent. Because of time constrains we may not be able to create a new AI and train it. This software will use php framework codeingiter to develop it in MVC structure. It also may be depended on some existing data of product list and price. 


<img src="wireframe/android/1.jpg" width="200">
![](wireframe/android/1.jpg =200x200)

| Day     | Meal    | Price |
| --------|---------|-------|
| Monday  | pasta   | $6    |
| Tuesday | chicken | $8    |



|name|id|
|----|--|
|sks|151|
|skp|161|